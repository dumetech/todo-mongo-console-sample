package core;

import java.net.UnknownHostException;
import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

/*
*   Author Sercan
*/

public class MongoTestUnit {
	
	private static List<String> databaseNames = new ArrayList<String>();
	
	public static void main(String[] args){
		
		
		menu();
	}
	
	//Kullanýcý icin konsol arayuzu hazýrlanýr.
	private static void menu(){
		boolean flag = true;
		int tercih = -1;
		DB selectedDatabase = null;
		
		DBCollection selectedTable = null;
		
		while(flag){
			//iţlem menusu kullanýcý icin hazýrlanýr.
			System.out.println("[1] Create Database");
			System.out.println("[2] Create table");
			System.out.println("[3] Insert New Row");
			System.out.println("[4] Show All Records");
			System.out.println("[5] Show Database Names");
			System.out.println("[6] Exit");
			//Scanner ile klavyeden basýlan degerler alýnýr.
			Scanner scanner = new Scanner(System.in);
			
			tercih = scanner.nextInt();
			
			// Tercihe göre Tablo oluţturma, Veritabaný oluţturma ve çýkýţ iţlemi yapýlýr. 
            switch (tercih) { 
                   case 1: 
                          // Veritabaný ismi girilir ve girilen isimde veritabaný yok ise yaratýlýr.
                          Scanner enterDatabase = new Scanner(System.in);                           
                          System.out.println("Olusturulacak olan veritabaný ismini giriniz :");
                          selectedDatabase = createDatabase(enterDatabase.next().trim()); 
                          break; 
                   case 2: 
                          // Seçili olan veya yaratýlmýţ veritabaný yok ise tablo yaratma iţlemi yapýlmaz.
                          if(selectedDatabase != null) { 
                        	    System.out.println("Tablo ismi giriniz : ");
                                Scanner enterTable = new Scanner(System.in);
                                selectedTable = createTable(selectedDatabase, enterTable.next().trim());
                          } 
                          else {
                                // Ekrana uyarý mesajý verilir.
                                System.out.println("Veritabaný seçilmeden tablo yaratýlamaz !");
                          } 
                          break;
                   case 3: 
                          if(selectedTable != null) {
                                InsertRow(selectedTable);
                          } 
                          else {
                                System.out.println("Tablo seçilmeden kayýt girilemez !");
                          } 
                          break;
                   case 4: 
                          if(selectedTable != null) {
                                showAllRows(selectedTable);
                          } 
                          else {
                                System.out.println("Tablo seçilmeden listeleme yapýlamaz !");
                          } 
                          break;
                   case 5: 
                          getDatabaseNames();
                          break; 
                   case 6: 
                       flag = false;
                       break; 
                   default: 
                          flag = false;

                          break; 
            }
		}
	}
	
	private static void getDatabaseNames(){
		Mongo mongoDbManager = null;
		try{
			mongoDbManager= new Mongo("localhost",27017);
			databaseNames = mongoDbManager.getDatabaseNames();			
		}catch(UnknownHostException e){
			e.printStackTrace();
		}
		
		for (String dbname : databaseNames) {
			System.out.println(dbname + "\n");		
		}
	}
 	


	private static DB createDatabase(String dbname){
		//Mongo sýnýf tipinde bir nesne ile MONGODB ile baglantý kurulur.
		
		Mongo mongoDbManager = null;
		DB db = null;
		
		try{
			//IP ve PORT bilgileri girilir.
			mongoDbManager = new Mongo("localhost",27017);
			db = mongoDbManager.getDB(dbname);
			
		}catch(UnknownHostException e){
			e.printStackTrace();
		}
		
		
		return db;
	}
	
	private static void InsertRow(DBCollection table){
		String username = "";
		String userSurname="";
		short userAge = -1;
		String userAddress = "";
		double xPosition = 0.0;
		double yPosition = 0.0;
		//Scanner ile kullanýcýdan degerler alýnýr.
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Adýnýzý giriniz :");
		username = scanner.next();
		
		System.out.println("Soyadýnýzý giriniz :");
		userSurname=scanner.next();
		
		System.out.println("Yaţýnýzý giriniz :");
		userAge = scanner.nextShort();
		
		System.out.println("Adresinizi giriniz :");
		userAddress = scanner.next();
		
		System.out.println("x koordinatýný giriniz : ");
		xPosition = scanner.nextDouble();
		
		System.out.println("y koordinatýný giriniz : ");
		yPosition = scanner.nextDouble();
		
		//BasicDBObject nesnesi ile key-value mantýgýnda bilgiler set edilir.
		
		BasicDBObject columns = new BasicDBObject();
		columns.put("name", username);
		columns.put("surname", userSurname);
		columns.put("age", userAge);
		columns.put("address", userAddress);
		
		// Pozisyon bilgileride eklenir. Bu Basic Object nesnesi "info" kolonu olacaktýr.
        BasicDBObject locationColumn = new BasicDBObject(); 
        locationColumn.put("xPosition", xPosition); 
        locationColumn.put("yPosition", yPosition);
        
        //Bütün bu iţlemlerden sonra JSON dosyasý 
        //columns.toString() ile JSON icerigini gorebilirsiniz.
        
        columns.put("info",locationColumn);
        table.insert(columns);
		
		
	}
	
	//Secilmis olan bir tabloya ait butun kolonlarý goruntuleme
	private static void	showAllRows(DBCollection table){
		//"find" metoduyla JSON olarak saklanan kayýtlarýn hepsi listelenir.
		//DBCursor nesnesi ile bu liste uzerinde hareket edilir.
		DBCursor cursor = table.find();
		while(cursor.hasNext()){
			//"cursor.next()" ile bir JSON dokumaný okunur.
			DBObject row = cursor.next();
			//Key-Value mantýgýnda calýstýgý icin Key Set alýnýr.
			//Ornegin name, surnam, info gibi key isimleri verilmisti.
			Set<String> keys = row.keySet();
			// Okunmuţ olan JSON kaydýna iliţkin bütün kolon bilgileri okunur ve ekranda gösterilir. 

            for (Iterator<?> iterator = keys.iterator(); iterator.hasNext();) { 

                   String key = (String) iterator.next(); 

                   System.out.println(row.get(key)); 

            } 

            System.out.println("---------------------------"); 

     }      

     cursor.close();
		}
	
	// Parametre olarak gönderilen veritabaný üzerinde bir tablo yaratýlýr. 
    private static DBCollection createTable(DB db, String tableName) { 

          // getCollection metoduyla yeni bir tablo yaratýlýr. 

          DBCollection table = db.getCollection(tableName); 

          return table; 

    }
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

